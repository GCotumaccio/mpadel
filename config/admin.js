module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '0de510c0a8ec2ee712b1963c3601ef6a'),
  },
});
